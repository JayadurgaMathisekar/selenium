package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC002_CreateLead extends ProjectMethods {
	
	@BeforeTest 
	public void setData{
		
		testCaseName = "TC_Create Lead";
		testCaseDesc="Create a New Lead";
		category="Smaoke";
		author="Jaya";
	}

	@Test	
	void createLead(){
		startApp("chrome","http://leaftaps.com/opentaps/control/main");
		WebElement user = locateElement("id", "username");
		type(user, "DemoSalesManager");
		WebElement pswd = locateElement("id", "password");
		type(pswd, "crmsfa");
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		WebElement crm = locateElement("linktext","CRM/SFA");
		click(crm);
		WebElement cld = locateElement("linktext","Create Lead");
		click(cld);
		WebElement cname = locateElement("id", "createLeadForm_companyName");
		type(cname, "Accenture");
		WebElement fname = locateElement("id", "createLeadForm_firstName");
		type(fname, "Jaya");
		WebElement lname = locateElement("id", "createLeadForm_lastName");
		type(lname, "Durga");
		WebElement dsour = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(dsour, "Direct Mail");
		WebElement ind = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingIndex(ind, 2);
		WebElement ownr = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingIndex(ownr, "OWN_PUBLIC_CORP");


	}

}
