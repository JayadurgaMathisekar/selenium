package MobileInterface;

public interface MobileInterface extends Interface2 {
	public void enableBluetooth();
	public void support5G();
	
}
