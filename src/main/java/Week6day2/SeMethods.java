����   4  #dataProviderAndParameters/SeMethods  utils/Reporter  #dataProviderAndParameters/WdMethods driver ,Lorg/openqa/selenium/remote/RemoteWebDriver; sUrl Ljava/lang/String; primaryWindowHandle sHubUrl sHubPort i I <init> ()V Code
    	     LineNumberTable LocalVariableTable this %LdataProviderAndParameters/SeMethods; startApp '(Ljava/lang/String;Ljava/lang/String;)V  chrome
   " ! java/lang/String # $ equalsIgnoreCase (Ljava/lang/String;)Z & webdriver.chrome.driver ( ./drivers/chromedriver
 * , + java/lang/System - . setProperty 8(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 0 'org/openqa/selenium/chrome/ChromeDriver
 / 	  3   5 firefox 7 webdriver.gecko.driver 9 ./drivers/geckodriver ; )org/openqa/selenium/firefox/FirefoxDriver
 : 
 > @ ? *org/openqa/selenium/remote/RemoteWebDriver A B get (Ljava/lang/String;)V
 > D E F manage )()Lorg/openqa/selenium/WebDriver$Options; H J I %org/openqa/selenium/WebDriver$Options K L window (()Lorg/openqa/selenium/WebDriver$Window; N P O $org/openqa/selenium/WebDriver$Window Q  
fullscreen H S T U timeouts *()Lorg/openqa/selenium/WebDriver$Timeouts;       	 Y [ Z java/util/concurrent/TimeUnit \ ] SECONDS Ljava/util/concurrent/TimeUnit; _ a ` &org/openqa/selenium/WebDriver$Timeouts b c implicitlyWait J(JLjava/util/concurrent/TimeUnit;)Lorg/openqa/selenium/WebDriver$Timeouts; e java/lang/StringBuilder g The browser:
 d i  B
 d k l m append -(Ljava/lang/String;)Ljava/lang/StringBuilder; o  launched successfully
 d q r s toString ()Ljava/lang/String; u pass
  w x  
reportStep z  could not be launched | fail ~ &org/openqa/selenium/WebDriverException browser url e (Lorg/openqa/selenium/WebDriverException; StackMapTable locateElement F(Ljava/lang/String;Ljava/lang/String;)Lorg/openqa/selenium/WebElement;
   � � � hashCode ()I � id
   � � � equals (Ljava/lang/Object;)Z � tag � name � class � xpath � linktext
 > � � � findElementById 4(Ljava/lang/String;)Lorg/openqa/selenium/WebElement;
 > � � � findElementByLinkText
 > � � � findElementByXPath
 > � � � findElementByName
 > � � � findElementByClassName
 > � � � findElementByTagName	 * � � � out Ljava/io/PrintStream; � The element with locator  �  and with value  �  not found.
 � � � java/io/PrintStream � B println � java/lang/RuntimeException
 �  � WebDriverException � *org/openqa/selenium/NoSuchElementException locator locValue ,Lorg/openqa/selenium/NoSuchElementException; type 5(Lorg/openqa/selenium/WebElement;Ljava/lang/String;)V � � � org/openqa/selenium/WebElement �  clear � java/lang/CharSequence � � � � sendKeys ([Ljava/lang/CharSequence;)V � 
The data:  �  entered successfully � Pass �  could not entered
 } � � s 
getMessage � 0org/openqa/selenium/InvalidElementStateException ele  Lorg/openqa/selenium/WebElement; data 2Lorg/openqa/selenium/InvalidElementStateException; click #(Lorg/openqa/selenium/WebElement;)V �   � ,org/openqa/selenium/support/ui/WebDriverWait       

 � �  � #(Lorg/openqa/selenium/WebDriver;J)V
 � � � 1org/openqa/selenium/support/ui/ExpectedConditions � � elementToBeClickable T(Lorg/openqa/selenium/WebElement;)Lorg/openqa/selenium/support/ui/ExpectedCondition;
 � � � � until 1(Ljava/util/function/Function;)Ljava/lang/Object; � � � s getText � � �  � The element :   is clicked successfully The element:   could not clicked text wait .Lorg/openqa/selenium/support/ui/WebDriverWait; clickWithNoSnap 4(Lorg/openqa/selenium/WebElement;)Ljava/lang/String; bReturn getTitle
 > s title getAttribute F(Lorg/openqa/selenium/WebElement;Ljava/lang/String;)Ljava/lang/String; � &(Ljava/lang/String;)Ljava/lang/String; 	attribute selectDropDownUsingText %org/openqa/selenium/support/ui/Select
  �
 B selectByVisibleText #The dropdown is selected with text  value selectDropDownUsingIndex $(Lorg/openqa/selenium/WebElement;I)V
$%& selectByIndex (I)V( $The dropdown is selected with index 
 d* l+ (I)Ljava/lang/StringBuilder; index selectDropDownUsingValue
/0 B selectByValue2 $The dropdown is selected with value  verifyTitle
 6 The expected title 8  matches the actual 
  :;< valueOf &(Ljava/lang/Object;)Ljava/lang/String;>  The expected title @  doesn't matches the actual B WebDriverException :  expectedTitle Z verifyExactText
 G �
I %The expected text matches the actual K -The expected text doesn't matches the actual  expectedText verifyPartialText
  OPQ contains (Ljava/lang/CharSequence;)ZS &The expected text contains the actual U -The expected text doesn't contain the actual  verifyExactAttribute G(Lorg/openqa/selenium/WebElement;Ljava/lang/String;Ljava/lang/String;)V
 Y[ The expected attribute :]  value matches the actual _ # value does not matches the actual  verifyPartialAttributeb  value contains the actual d $ value does not contains the actual  verifySelected �ghi 
isSelected ()Zk The element 
 dm ln -(Ljava/lang/Object;)Ljava/lang/StringBuilder;p  is selectedr  is not selected verifyDisplayed �uvi isDisplayedx  is visiblez  is not visible switchToWindow
 >}~ getWindowHandles ()Ljava/util/Set;� java/util/ArrayList
� ��� java/util/List�� addAll (Ljava/util/Collection;)Z
 >��� switchTo /()Lorg/openqa/selenium/WebDriver$TargetLocator;�� A� (I)Ljava/lang/Object;��� +org/openqa/selenium/WebDriver$TargetLocator K� 3(Ljava/lang/String;)Lorg/openqa/selenium/WebDriver;� 7The driver could not move to the given window by index � )org/openqa/selenium/NoSuchWindowException allWindowHandles Ljava/util/Set; 
allHandles Ljava/util/List; +Lorg/openqa/selenium/NoSuchWindowException; LocalVariableTypeTable #Ljava/util/Set<Ljava/lang/String;>; $Ljava/util/List<Ljava/lang/String;>; switchToFrame���� frame A(Lorg/openqa/selenium/WebElement;)Lorg/openqa/selenium/WebDriver;� switch In to the Frame 
� �� (org/openqa/selenium/NoSuchFrameException *Lorg/openqa/selenium/NoSuchFrameException; acceptAlert���� alert ()Lorg/openqa/selenium/Alert;� �� org/openqa/selenium/Alert���  accept� 
The alert �  is accepted.� There is no alert present.� +org/openqa/selenium/NoAlertPresentException Lorg/openqa/selenium/Alert; -Lorg/openqa/selenium/NoAlertPresentException; dismissAlert���  dismiss�  is dismissed. getAlertText takeSnap	��� org/openqa/selenium/OutputType�� FILE  Lorg/openqa/selenium/OutputType;
 >��� getScreenshotAs 4(Lorg/openqa/selenium/OutputType;)Ljava/lang/Object;� java/io/File� ./snaps/snap� .jpg
� i
��� org/apache/commons/io/FileUtils�� copyFile (Ljava/io/File;Ljava/io/File;)V� The browser has been closed.� The snapshot could not be taken� java/io/IOException Ljava/io/IOException; explicitWait $(ILorg/openqa/selenium/WebElement;)V time� The Element �  is cleared Successfully
 ��  closeBrowser
 >��  close� The browser is closed� The browser could not be closed� java/lang/Exception Ljava/lang/Exception; closeAllBrowsers
 >��  quit  The opened browsers are closed  The browsers could not be closed 
SourceFile SeMethods.java InnerClasses org/openqa/selenium/WebDriver Options TargetLocator Timeouts Window !      	      	 
     
     
     
                 <     
*� *� �           $  ( 	 $        
           3     �+� � %'� )W� /Y� 1� 2� +4� � 68� )W� :Y� <� 2� 2,� =� 2� C� G � M � 2� C� R  V� X� ^ W*� dYf� h+� jn� j� pt� v� N*� dYf� h+� jy� j� p{� v�    � � }     6    , 	 -  .  / ' 0 / 1 9 3 @ 4 P 5 g 6 � 7 � 8 � ;    *    �       �  
    � � 
  �  � �  �    � K }  � �    �     �+YN� ��    �        : ��   F 3z�   R��x   ^�h�   jG<h'   v-�� �� B� �-�� �� ^� �-�� �� B� �-�� �� >� �-�� �� "� x-�� �� � l� 2,� ��� 2,� ��� 2,� ��� 2,� ��� 2,� ��� 2,� ��� 9N� �� dY�� h+� j�� j,� j�� j� p� �� �Y� ��N� ��� ��    � � � � � � � � � � � � � � � � � � � � � � �   � � } � � � } � � � } � � � } � � � } � � � }     6    B � D � E � F � G � H � I � K � M � N � O � P � R    4    �       � � 
    � � 
  � , � �  �  � �  �    � @  � B �l }  � �     <     � 2+� ��           V                � 
   � �     �     l+� � +� �Y,S� � *� dYӷ h,� jն j� p׶ v� <N� �� dYӷ h,� jٶ j� p� �� N� �� dY�� h-� ۶ j� p� ��    / 2 �   / Q }     "    \  ]  ^ / _ 3 ` Q a R b k d    4    l       l � �    l � 
  3  � �  R  � �  �    r �^ }  � �    6     ��M� �Y� 2 � �N-+� � �W+� � M+� � *� dY�� h,� j � j� pt� v� >N� �� dY� h,� j� j� p� �� N� �� dY�� h-� ۶ j� p� ��   C F �  C g }     .    g  i  j  k ! l ' m C n G o g p h q � t    >    �       � � �    
   2  G  � �  h  � �  �    � F   �    �` } 	 �    6     ��M� �Y� 2 � �N-+� � �W+� � M+� � � �� dY�� h,� j � j� p� �� >N� �� dY� h,� j� j� p� �� N� �� dY�� h-� ۶ j� p� ��   C F �  C g }     .    w  y  z  { ! | ' } C ~ G  g � h � � �    >    �       � � �    
   2  G  � �  h  � �  �    � F   �    �` }  �
     �     )�M+� � M� N� �� dY�� h-� ۶ j� p� �,�   
  }         �  � 
 �  � ' �    *    )       ) � �   & 
    � �  �    �    �    }  s     �     )�L� 2�L� M� �� dY�� h,� ۶ j� p� �+�   
  }         �  � 
 �  � ' �         )      & 
    � �  �    �       }      �     ,�N+,� N� :� �� dY�� h� ۶ j� p� �-�     }         �  �  �  � * �    4    ,       , � �    , 
   ) 
    � �  �    �    �      }  �     �     A�Y+�,�� �� dY� h,� j� p� �� N� �� dY�� h-� ۶ j� p� ��    # & }         �  � # � ' � @ �    *    A       A � �    A  
  '  � �  �    f } !"     �     A�Y+��#� �� dY'� h�)� p� �� N� �� dY�� h-� ۶ j� p� ��    # & }         �  � # � ' � @ �    *    A       A � �    A,   '  � �  �    f } - �     �     A�Y+�,�.� �� dY1� h,� j� p� �� N� �� dY�� h-� ۶ j� p� ��    # & }         �  � # � ' � @ �    *    A       A � �    A  
  '  � �  �    f } 3 $         �=*�4+� �� ,� �� dY5� h+� j7� j*�4� j� p� �=� O� �� dY*�4�9� h=� j+� j?� j*�4� j� p� �� N� �� dYA� h-� ۶ j� p� ��   d g }     & 	   �  �  � 1 � 3 � 6 � d � h � � �    *    �       �C 
   �D  h  � �  �    � 6p } E �     �     \*+�F,� �� � �� dYH� h,� j� p� �� 8� �� dYJ� h,� j� p� �� N� �� dYA� h-� ۶ j� p� ��    = @ }         �  � # � & � = � A � [ �    *    \       \ � �    \L 
  A  � �  �    &Y } M �     �     \*+�F,�N� � �� dYR� h,� j� p� �� 8� �� dYT� h,� j� p� �� N� �� dYA� h-� ۶ j� p� ��    = @ }         �  � # � & � = � A � [ �    *    \       \ � �    \L 
  A  � �  �    &Y } VW     �     s*+,�X-� �� '� �� dYZ� h,� j\� j-� j� p� �� D� �� dYZ� h,� j^� j-� j� p� ��  :� �� dYA� h� ۶ j� p� ��    R U }         �  � . � 1 � R � W � r �    4    s       s � �    s 
    s  
  W  � �  �    1c } `W     �     s*+,�X-�N� '� �� dYZ� h,� ja� j-� j� p� �� D� �� dYZ� h,� jc� j-� j� p� ��  :� �� dYA� h� ۶ j� p� ��    R U }         �  � . � 1 � R � W  r    4    s       s � �    s 
    s  
  W  � �  �    1c } e �     �     e+�f � #� �� dYj� h+�lo� j� p� �� >� �� dYj� h+�lq� j� p� �� M� �� dYA� h,� ۶ j� p� ��    F I }         	 & )	 F J d         e       e � �  J  � �  �    )_ } s �     �     e+�t � #� �� dYj� h+�lw� j� p� �� >� �� dYj� h+�ly� j� p� �� M� �� dYA� h,� ۶ j� p� ��    F I }         	 & ) F J d         e       e � �  J  � �  �    )_ } {&          g� 2�|M��Y��N-,�� W� 2��-�� �  �� W� 9M� �� dY�� h�)� p� �� M� �� dYA� h,� ۶ j� p� ��    - 0�   - K }     & 	      ! -" 1# K$ L% f'    >    g       g,    &��   ��  1  ��  L  � � �      &��   ��  �    p�Z } � �     �     a� 2��+�� W� �� dY�� h+�l� p� �� <M� �� dYA� h,��� j� p� �� M� �� dYA� h,� ۶ j� p� ��    $ '�   $ E }        + , $- (. E/ F0 `2    *    a       a � �  (  ��  F  � �  �    g�] } �          e�L� 2���� M,�� L,�� � �� dY�� h+� j�� j� p� �� +M� ��� �� M� �� dYA� h,� ۶ j� p� ��   9 <�  9 I }     * 
  5 7 8 9 : 9; =< I= J> d@    4    e      b 
   *��  = 	 ��  J  � �  �    � <     �L } �          e�L� 2���� M,�� L,�� � �� dY�� h+� jŶ j� p� �� +M� ��� �� M� �� dYA� h,� ۶ j� p� ��   9 <�  9 I }     * 
  C E F G H 9I =J IK JL dO    4    e      b 
   *��  = 	 ��  J  � �  �    � <     �L } � s     �     C�L� 2���� M,�� L� +M� ��� �� M� �� dYA� h,� ۶ j� p� �+�    �   & }     "   R T U V W &X 'Y A[    4    C      @ 
   ��   	 ��  '  � �  �    �      �L } �      �     K� 2�ɶ��ӻ�Y� dYշ h*� �)׶ j� p�ٸڧ L� �� �� L� �� ��    0 3 }   0 @�        a 0b 4c @d Ae Jh         K     4 	 � �  A 	 ��  �    s }L�	 ��     u     � �Y� 2�� �N-,� � �� �:�          k l n    4           �      � �       � �   � �     h     (+� � � �� dY� h+�l� j� p� �*��          q r #s 't        (       ( � �  �      v     � 2�� ��� �� L� ��� ��     �        y z { | ~              	 ��  �    R�	 �      v     � 2��� ��� �� L� �� ��     �        � � � � �              	 ��  �    R�	       "  H	�		 _
	 N	