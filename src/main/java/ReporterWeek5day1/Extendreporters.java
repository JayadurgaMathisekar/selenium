package ReporterWeek5day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Extendreporters {

	public static void main(String[] args) throws IOException {

		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		ExtentTest test=extent.createTest("TC001_CreateLead","Create a new Lead");
		test.pass("Browser Launched successfully");
		test.pass("The data DemosalesManager entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("The data crmsfa entered successfully");
		test.pass("The element Login button clicked on successfully");
		extent.flush();


	}

}
