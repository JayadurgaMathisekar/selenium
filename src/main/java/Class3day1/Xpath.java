package Class3day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Xpath {

	public static void main(String[] args) { 
		
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/table.html");
		List<WebElement> elementsByXPath = driver.findElementsByXPath("//input[@type='checkbox']");
		System.out.println(elementsByXPath.size());
		//WebElement webElement = elementsByXPath.get(2);
		//webElement.click();
		
		//WebElement table = driver.findElementByXPath("//section[@class='innerblock']//table");
		WebElement table2 = driver.findElementByXPath("(//input[@type='checkbox'])[2]");
		table2.click();
		//List<WebElement> row = table.findElements(By.className("tr"));
		//List<WebElement> column = row.get(2).findElements(By.tagName("td"));
		//table2.get(2).click();
	}

}
